package main

import (
	"fmt"
	"net/http"

	"bitbucket.org/bilgee0629/excel/cmd"
)

func main() {
	http.HandleFunc("/", sayHello)
	http.HandleFunc("/generate_reports", generateReports)
	if err := http.ListenAndServe(":8080", nil); err != nil {
		panic(err)
	}
}

func sayHello(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("hello world. server is running!"))
}

var emailAddr = "EMAIL_ADDR_TO_SEND_TO"

func generateReports(w http.ResponseWriter, r *http.Request) {
	franchises, err := cmd.GetFranchises("URL HERE")
	if err != nil {
		w.Write([]byte(fmt.Sprintf("error while fetching franchises: %v", err)))
		return
	}

	// creating a dir where reports will go into
	dirName := cmd.CreateDir()

	fileNames := []string{}
	for _, franchise := range franchises {
		// fetching a franchise data
		franchiseData, err := cmd.FetchFranchiseData(franchise.ID)
		if err != nil {
			w.Write([]byte(fmt.Sprintf("error while fetching franchise data: %v", err)))
			return
		}

		// generating report
		fileName, err := cmd.GenerateReport(franchiseData, franchise.ID, dirName)
		if err != nil {
			w.Write([]byte(fmt.Sprintf("error while generating report: %v", err)))
			return
		}
		fileName = dirName + "/" + fileName // registering file in the newly created dir
		fileNames = append(fileNames, fileName)
	}

	// zipping files
	zipName := dirName + ".zip"
	if err := cmd.ZipFiles(zipName, fileNames); err != nil {
		w.Write([]byte(fmt.Sprintf("error while zipping files: %v", err)))
		return
	}

	// Enter your domain, mailgun api key, email address to send to, and zipname
	_, err = cmd.SendAttachment("DOMAIN", "APIKEY", emailAddr, zipName)
	if err != nil {
		w.Write([]byte(fmt.Sprintf("error while sending zipped file: %v", err)))
		return
	}

	w.Write([]byte(fmt.Sprintf("Successfully sent reports to %s", emailAddr)))
}
