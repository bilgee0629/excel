package cmd

import (
	"encoding/json"
	"net/http"
)

type Franchise struct {
	ID int `json:"franchise_id"`
}

func GetFranchises(endpoint string) ([]Franchise, error) {
	res, err := http.Get(endpoint)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	var franchises []Franchise
	if err := json.NewDecoder(res.Body).Decode(&franchises); err != nil {
		return nil, err
	}

	return franchises, nil
}

type FranchiseData struct {
	CustomerID         int    `json:"customer_id"`
	CurrentStatus      string `json:"current_status"`
	CurrentStatusNotes string `json:"current_status_notes"`
	ReferredData       string `json:"referred_date"`
	ReferrerName       string `json:"referrer_name"`
	ProductsOfInterest string `json:"products_of_interest"`
}

var franchiseDataURL = ""

func FetchFranchiseData(franchiseID int) ([]FranchiseData, error) {
	res, err := http.Get(franchiseDataURL)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	var franchiseData []FranchiseData
	if err := json.NewDecoder(res.Body).Decode(&franchiseData); err != nil {
		return nil, err
	}

	return franchiseData, nil
}
