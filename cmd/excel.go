package cmd

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/tealeg/xlsx"
)

func GenerateReport(data []FranchiseData, franchiseID int, dirName string) (string, error) {
	dateMap := make(map[string][]FranchiseData)
	for _, item := range data {
		date, err := ParseDateFromString(item.ReferredData)
		if err != nil {
			return "", err
		}
		sheetName := GenerateSheetName(*date)
		dateMap[sheetName] = append(dateMap[sheetName], item)
	}

	excelFile := xlsx.NewFile()
	for sheetName, data := range dateMap {
		sheet, err := excelFile.AddSheet(sheetName)
		if err != nil {
			return "", err
		}

		// adding field names to the sheet
		FillFieldNamesRow(sheet)

		// adding data as rows to the sheet
		for _, item := range data {
			FillDataRow(sheet, item)
		}
	}

	fileName := fmt.Sprintf("%s/%d.xlsx", dirName, franchiseID)
	if err := excelFile.Save(fileName); err != nil {
		panic(fmt.Errorf("Error saving excel file: %s", err))
	}
	return fileName, nil
}

// CreateDir creates a new directory and returns directory name
func CreateDir() string {
	dirName := strconv.Itoa(int(time.Now().UnixNano()))
	os.Mkdir("./"+dirName, os.FileMode(0700))
	return dirName
}

func FillFieldNamesRow(sheet *xlsx.Sheet) {
	row := sheet.AddRow()
	fieldNames := []string{"Client", "Referrer", "Date Referred", "Product", "Status", "Note"}
	for _, name := range fieldNames {
		cell := row.AddCell()
		cell.Value = name
	}
}

// fills a row with franchise data
func FillDataRow(sheet *xlsx.Sheet, data FranchiseData) {
	row := sheet.AddRow()
	FillDataCell(row, strconv.Itoa(data.CustomerID))
	FillDataCell(row, data.ReferrerName)
	FillDataCell(row, data.ReferredData)
	FillDataCell(row, data.ProductsOfInterest)
	FillDataCell(row, data.CurrentStatus)
	FillDataCell(row, data.CurrentStatusNotes)
}

// Fills a cell with given data field
func FillDataCell(row *xlsx.Row, cellData string) {
	cell := row.AddCell()
	cell.Value = cellData
}

var dateLayout = "02-01-2006" // mm-dd-yy
func ParseDateFromString(date string) (*time.Time, error) {
	parsedDate, err := time.Parse(dateLayout, date)
	if err != nil {
		return nil, err
	}
	return &parsedDate, nil
}

var sheetDateLayout = "Jan 2006"

func GenerateSheetName(date time.Time) string {
	return date.Format(sheetDateLayout)
}
